package in.nareshit.gourav.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import in.nareshit.gourav.exception.CompanyNotFoundException;
import in.nareshit.gourav.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {

	/**
	 * If CompanyNotFoundException is thrown from any RestController
	 * then below method is executed and Returns Error Message with 500 Status code.
	 * It is like a Reusable Catch block code.
	 */
	@ExceptionHandler(CompanyNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleCompanyNotFoundException(
			CompanyNotFoundException cnfe)
	{
		return ResponseEntity.internalServerError().body(
				new ErrorMessage(
						new Date().toString(), 
						cnfe.getMessage(), 
						HttpStatus.INTERNAL_SERVER_ERROR.value(), 
						HttpStatus.INTERNAL_SERVER_ERROR.name()
						)
				
				);
	}
}
