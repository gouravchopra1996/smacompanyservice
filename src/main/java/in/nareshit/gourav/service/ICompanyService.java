package in.nareshit.gourav.service;

import java.util.List;

import in.nareshit.gourav.entity.Company;

public interface ICompanyService {

	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();
	void deleteCompany(Long id);
}
