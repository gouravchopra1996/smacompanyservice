package in.nareshit.gourav.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nareshit.gourav.entity.Company;
import in.nareshit.gourav.exception.CompanyNotFoundException;
import in.nareshit.gourav.repo.CompanyRepository;
import in.nareshit.gourav.service.ICompanyService;

@Service
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	private CompanyRepository repo;
	
	@Override
	public Long createCompany(Company cob) {
		return repo.save(cob).getId();
	}

	@Override
	public void updateCompany(Company cob) {
		if(cob.getCregId()!=null && repo.existsById(cob.getId()))
			repo.save(cob);
	}

	@Override
	public Company getOneCompany(Long id) {
		Optional<Company> opt = repo.findById(id);
		if(opt.isEmpty()) {
			throw new CompanyNotFoundException("Given id '"+id+"' Not exist");
		} else {
			return opt.get();
		}
	}

	@Override
	public List<Company> getAllCompanies() {
		return repo.findAll();
	}

	@Override
	public void deleteCompany(Long id) {
		repo.deleteById(id);
	}

}
