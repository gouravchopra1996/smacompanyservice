package in.nareshit.gourav.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.gourav.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}
